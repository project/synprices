<?php

namespace Drupal\synprices\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for syncommerce routes.
 */
class SynpricesController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $tid = \Drupal::request()->query->get('term_product_tid_depth');
    $view = [
      '#type' => 'view',
      '#name' => 'prices',
      '#display_id' => 'block_prices',
      '#arguments' => $tid,
      '#embed' => TRUE,
    ];
    return $view;
  }

}
